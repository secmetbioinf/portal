# README #

Git repo for Secondary Metabolite Bioinformatics portal

### What is this repository for? ###

* A repository for links to important tools and databases for Secondary Metabolite Research

### How do I get set up? ###

* Install mkdocs
* Get portal data from git
```
git clone git@bitbucket.org:secmetbioinf/portal.git portal
```

* For testing:

```
mkdocs serve
```

	
(this will start a test server on 127.0.0.1:8000)

* For rollout
	
```
mkdocs build
```
This generates the build directory, which just has to be copied to a web server.


### Contact
Tilmann Weber

Novo Nordisk Foundation Center for Biosustainability

Technical University of Denmark

Email: tiwe@biosustain.dtu.dk
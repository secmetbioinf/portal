# Genome-scale Metabolic Modeling Tools

---

[TOC]

---

## antiSMASH

please see [antiSMASH](antismash.md) page.

---

## BiGMeC

BiGMeC (**Bi**osynthetic **G**ene cluster **Me**tabolic pathway **C**onstruction) is a pipeline that leverages
antiSMASH results to create the metabolic pathway corresponding to a PKS or NRPS biosynthetic gene cluster.

Year published: 2021

Link:

* <https://github.com/AlmaasLab/BiGMeC>

Reference:

* [Sulheim, S., Fossheim, F.A., Wentzel, A. et al., 2021, BMC Bioinform, 22:81](https://pubmed.ncbi.nlm.nih.gov/33622234)


---

## CoReCo

CoReCo (**Co**mparative **ReCo**nstruction) is useful for modeling metabolisms of multiple related species.

Year published: 2014

Link:

* <https://github.com/esaskar/CoReCo>

Reference:

* [Pitkanen, E., et al., 2014, PLoS Comput. Biol. 10:e1003465](http://www.ncbi.nlm.nih.gov/pubmed/24516375)


---

## FAME

FAME (The **F**lux **A**nalysis and **M**odeling **E**nvironment) allows streamlined analysis of a newly built metabolic model using various simulation methods.

Year published: 2012

Link:

* <http://f-a-m-e.fame-vu.vm.surfsara.nl/ajax/page1.php>

Reference:

* [Boele, J., et al., 2012, BMC Syst. Biol. 6:8](http://www.ncbi.nlm.nih.gov/pubmed/22289213)

---

## GEMSiRV

GEMSiRV (**GE**nome-scale **M**etabolic models **Si**mulation, **R**econstruction and **V**isualization) allows metabolic model reconstruction, simulation and visualization.

Year published: 2012

Link:

* <http://sb.nhri.org.tw/GEMSiRV/en/GEMSiRV>

Reference:

* [Liao, Y. C., et al., 2012, Bioinformatics 28:1752-8](http://www.ncbi.nlm.nih.gov/pubmed/22563070)

---

## MEMOSys

MEMOSys (**ME**tabolic **MO**del research and development **Sys**tem) allows management, storage, and development of metabolic models.

Year published: 2011

Link:

* <https://memosys.i-med.ac.at/MEMOSys/home.seam>

Reference:
* [Pabinger, S., et al., 2011, BMC Syst. Biol. 5:20](http://www.ncbi.nlm.nih.gov/pubmed/21276275)

---

## merlin

*merlin* (Metabolic Models Reconstruction Using Genome-Scale Information) is the most recently released metabolic modeling program with comprehensive genome annotation functionalities necessary for model generation.

Year published: 2015

Link:

* <http://www.merlin-sysbio.org/>

Reference: 

* [Dias, O., et al., 2015, Nucleic Acids Res. 43:3899-910](http://www.ncbi.nlm.nih.gov/pubmed/25845595)


## MetaFlux in Pathway Tools

MetaFlux in Pathway Tools provides strong supports for predicting, modeling, curating and visualizing metabolic pathways in connection with various functionalities of Pathway Tools.

Year published: 2012

Link: 

* <http://bioinformatics.ai.sri.com/ptools/>

Reference: [Latendresse, M., et al., 2012, Bioinformatics 28:388-96](http://www.ncbi.nlm.nih.gov/pubmed/22262672)

---

## MicrobesFlux

MicrobesFlux (a semi-automatic, web-based platform) allows both flux balance analysis (FBA) and dynamic FBA of a newly generated metabolic model.

Year published: 2012

Link:

* <http://www.microbesflux.org/>

Reference:

* [Feng, X., et al., 2012, BMC Syst. Biol. 6:94](http://www.ncbi.nlm.nih.gov/pubmed/22857267)

---

## Model SEED

Model SEED is the first online high-throughput metabolic modeling tool.

Year published: 2010

Reference:

* [Henry, C. S., et al., 2010, Nat. Biotechnol. 28:977-82](http://www.ncbi.nlm.nih.gov/pubmed/20802497)

Link:

* <http://seed-viewer.theseed.org/seedviewer.cgi?page=ModelView>

---

## Mueller et al.

A high-throughput metabolic modeling framework was developed and used to reconstruct metabolic models of Cyanobacteria species. The framework is not available online.

Year published: 2013

Reference:

* [Mueller, T. J., et al., 2013, BMC Syst. Biol. 7:142](http://www.ncbi.nlm.nih.gov/pubmed/24369854)

---

## RAVEN Toolbox

RAVEN (**R**econstruction, **A**nalysis and **V**isualization of M**e**tabolic **N**etworks) Toolbox allows metabolic model reconstruction, simulation and visualization in MATLAB environment.

Year published: 2013

Link: 

* <http://biomet-toolbox.org/index.php?page=downtools-raven>

Reference:

* [Agren, R., et al., 2013, PLoS Comput. Biol. 9:e1002980](http://www.ncbi.nlm.nih.gov/pubmed/23555215)

---

## SuBliMinaL Toolbox

SuBliMinaL Toolbox has strengths in managing chemical information for metabolites in a metabolic model.

Year published: 2011

Link: 

* <http://www.mcisb.org/resources/subliminal/>

Reference:

* [Swainston, N., et al., 2011, J Integr Bioinform 8:186](http://www.ncbi.nlm.nih.gov/pubmed/22095399)

---

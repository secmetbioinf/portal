# Metabolomics / Computational mass spectormetry based tools

---

[TOC]

---

## GNPS / DEREPLICATOR

The Global Natural Products Social (GNPS) Molecular Networking site is a community driven portal for MS/MS networking based compound dereplication and identification. It offers various options, including commuity-curated spectral libraries for dereplication, data analysis tools (including continuous identification service, which re-analyses uploaded MS/MS data regularly, when the databases were updated), repositories to store the datasets and many more. GNPS is developed and maintained by the Dorrestein lab and colleagues at UCSD.
Identification of known natural products from the LC/MS data is done using the Dereplicator algorithm.

References:

* [Carver, W.M., et al., 2016, Nat. Biotechnol. 34:828-837](https://www.ncbi.nlm.nih.gov/pubmed/27504778)
* [Mohimani, H. et al., 2016, Nat. Chem. Biol. 13:30-37](https://www.ncbi.nlm.nih.gov/pubmed/27820803)

Links:

* GNPS: <http://gnps.ucsd.edu/>
* DEREPLICATOR (standalone): <http://cab.spbu.ru/software/dereplicator/>

---

## GNP / iSNAP Database

GNP (Genes to Natural Products) is an integrated platform to link gene cluster data (for PKS/NRPS clusters) to LC-MS/MS data.
TLC-MS/MS data is screened against precomputed databases or against a user-defined database generated from from predicted products of an [GNP / Genome Search](mining/#gnp-genome-search) run.

References:

* [Johnston, C.W., et al., 2015, Nat. Commun. 6:8421](http://www.ncbi.nlm.nih.gov/pubmed/26412281)
* [Yang, L., et al., 2015, Chem. Biol. 22:1259-69](http://www.ncbi.nlm.nih.gov/pubmed/26364933)
* [Ibrahim, A., et al., 2012, Proc. Natl. Acad. Sci. U. S. A. 109:19196-201](http://www.ncbi.nlm.nih.gov/pubmed/23132949)

Links:

* GNP: <http://magarveylab.ca/gnp/>
* iSNAP: <https://magarveylab.ca/analog/>
	(Link not functional at the moment)
	
---

## GRAPE / GARLIC - Generalized retro-biosynthetic assembly prediction engine

GRAPE is a pipeline, that matches unknown gene clusters (as identified with [PRISM](mining/#prism-gnp) with small molecule data from chemical databases. Monomer units of small molecules are automatically identified by GRAPE and then matched against the gene cluster data using the GARLIC algorithm.

Reference:

* [Dejong, C.A., et al., 2016, Nat. Chem. Biol. 12:1007-1014](https://www.ncbi.nlm.nih.gov/pubmed/27694801)

Links:

* GARLIC (web): <http://www.magarveylab.ca/garlic>
	(Link not functional at the moment)
* GARLIC (source code): <https://github.com/magarveylab/garlic-release>
* GRAPE (source code): <https://github.com/magarveylab/grape-release>
	
---

## NRPquest / RiPPquest / Cycloquest

Web based tools, which allow the detection of NRPs (NRPquest), RiPPs (RiPPquest) or cyclic peptides (Cycloquest) in MS/MS datasets. 

References:

* [Mohimani, H., et al., 2014, ACS Chem. Biol. 9:1545-51](http://www.ncbi.nlm.nih.gov/pubmed/24802639)
* [Mohimani, H., et al., 2014, J. Nat. Prod. 77:1902-9](http://www.ncbi.nlm.nih.gov/pubmed/25116163)
* [Mohimani, H., et al., 2011, J. Proteome Res. 10:4505-12](http://www.ncbi.nlm.nih.gov/pubmed/21851130)

Link:

* <http://cyclo.ucsd.edu>

---

## Pep2Path

Pep2Path is a python-based tool to connect predicted peptide sequences (for RiPPs and NRPs) that are deduced from genomic data to peptide mass fragments observed from MS/MS studies.

Reference:

* [Medema, M. H., et al., 2014, PLoS Comput. Biol. 10:e1003822](http://www.ncbi.nlm.nih.gov/pubmed/25188327)

Link:

* <http://pep2path.sourceforge.net>

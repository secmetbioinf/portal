# Tools to analyze PKS and NRPS megasynthase domain organization

---

[TOC]

---

## antiSMASH

please see [antiSMASH](antismash) page

---

## ClustScan Professional

ClustScan Professional is a is a commercial Java-based tool to identify PKS and NRPS gene clusters in genomic data. A 30-days evaluation license is available.

Reference:

* [Starcevic, A., et al., 2008, Nucleic Acids Res. 36: 6882-6892](http://www.ncbi.nlm.nih.gov/pubmed/18978015)

Link:

* <http://bioserv.pbf.hr/cms/index.php?page=clustscan>

---

## GNP/PRISM

please see [GNP /PRISM](mining/#gnp-prism) in the [Tools for cluster mining](mining) section

---

## MAPSI/MAPSIdb/ASMPK

The MAPSI System can be used to identify PKS domain in the PKS megasynthase sequences.

References:

* [Tae, H., et al., 2007, BMC Bioinformatics 8: 327](http://www.ncbi.nlm.nih.gov/pubmed/17764579)

* [Tae, H., et al., 2009, J. Microbiol. Biotechnol. 19: 140-146](http://www.ncbi.nlm.nih.gov/pubmed/19307762)

* [Tae, H., et al., 2009, Bioprocess Biosyst. Eng. 32: 723-7](http://www.ncbi.nlm.nih.gov/pubmed/19205748)


Link:

* <http://gate.smallsoft.co.kr:8008/pks/>
	(Currently not working - Server not found)
---

## NaPDoS 

The **Na**tural **P**roducts **Do**main **S**eeker uses a phylogenetic approach to identify and classify Condensation domains of NRPS and KS domains of PKS enzymes.

Reference: 

* [Ziemert, N., et al., 2012, PLoS ONE 7: e34064](http://www.ncbi.nlm.nih.gov/pubmed/22479523)

Link:

* <http://napdos.ucsd.edu/>

---

## NP.searcher

NP.searcher is a tool to identify type I PKS, NRPS and mixed PKS/NRPS in genomic data and use the information on the identified catalytic domains to predict structures of the metabolites encoded by the BGCs

References:

* [Li, M. H., et al., 2009, BMC Bioinformatics 10: 185](http://www.ncbi.nlm.nih.gov/pubmed/19531248)

Link:

* <http://dna.sherman.lsi.umich.edu/> 

---
## PKS/NRPS Web Server/Predictive Blast Server

please see [Specificity prediction for NRPS](NRPSspec/#pksnrps-web-serverpredictive-blast-server).

---

## SEARCHPKS/NRPS-PKS/SBSPKS

SEARCHPKS was one of the first automated tools able to automatically identify the catalytic domains of PKS megasyntase enzymes. In 2004, SEARCHPKS was integrated in the more comprehensive NRPS-PKS / SBSPKS framework. For PKS, SBSPKS also contains function to generate homology model and docking interface analyses. NRPS-PKS also contains a database of representative PKS and NRPS pathways.

References:

* [Yadav, G., et al., 2003, Nucleic Acids Res. 31: 3654-8](http://www.ncbi.nlm.nih.gov/pubmed/12824387)

* [Ansari, M. Z., et al., 2004, Nucleic Acids Res. 32: W405-13](http://www.ncbi.nlm.nih.gov/pubmed/15215420)

* [Anand, S., et al., 2010, Nucleic Acids Res. 38: W487-96](http://www.ncbi.nlm.nih.gov/pubmed/20444870)


Link:

* <http://linux1.nii.res.in/~pksdb/DBASE/pagesearchpks.html>

* <http://www.nii.ac.in/~pksdb/sbspks/master.html>

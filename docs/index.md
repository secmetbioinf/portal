# Welcome to www.secondarymetabolites.org



## The Secondary Metabolite Bioinformatics Portal

Welcome to the new portal. Here you will find information on all aspects of Secondary
Metabolite Bioinformatics, including hand-curated linkts to all major tools and databases
commonly used in the field

---

## antiSMASH

You are looking for our antiSMASH web-service? Please click [here](antismash.md).

Click <http://antismash.secondarymetabolites.org> for a direct link to the service.

---

## antiSMASH database

You are looking for our antiSMASH-database? Click <http://antismash-db.secondarymetabolites.org> for a direct link to the service.

---

## Contribution

We are happy about new contributors! Should you find any wrong or outdated information, or feel that 
a tools/database is missing, please contact Tilmann Weber at <tiwe@biosustain.dtu.dk>.

---

### Contributors (in alphabetical order)

* Kai Blin (DTU)
* Hyun Uk Kim (DTU / KAIST)
* Tilmann Weber (DTU)

---

### Citation

* Weber, T., and Kim, H.U. (2016). The secondary metabolite bioinformatics portal: Computational tools to facilitate synthetic biology of secondary metabolite production. Synth. Syst. Biotechnol. 1:69-79, doi: 10.1016/j.synbio.2015.1012.1002. [Link to full text](http://www.sciencedirect.com/science/article/pii/S2405805X15300156)

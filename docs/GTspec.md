# Tools to analyze glycosyltransferase specificities

---

[TOC]

---

## SEARCHGTr  

SEARCHGTr is a web tool to allow substrate predictions of glycosyl transferases involved in secondary metabolite biosynthetic gene clusters. 
Reference:

* [Kamra, P., et al., 2005, Nucleic Acids Res. 33: W220-W225](https://www.ncbi.nlm.nih.gov/pubmed/15980457)


Link:

* <http://linux1.nii.res.in/~pankaj/gt/gt_DB/html_files/searchgtr.html>
	(link currently is non functional)
	
* <http://www.nii.res.in/searchgtr.html>
	(link currently is non functional)

Keywords:
glycosyltransferase, predictor



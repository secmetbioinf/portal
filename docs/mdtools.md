# Welcome to MkDocs

For full documentation visit [mkdocs.org](http://mkdocs.org).

## Markdown syntax

* <http://daringfireball.net/projects/markdown/syntax>
* <https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet>

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs help` - Print this help message.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.

Testheading
===========

Testsubheading
--------------


| Column1   | Column2   | Column3   | Column4   |
|-|-|-|-|
| testdata1 | testdata2 | testdata3 | testdata4 |
| testdata5 | testdata6 | testdata7 | testdata8 |
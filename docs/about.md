# About this portal

This portal is maintaned by the New Bioactive Compounds Section at the [Novo Nordisk Foundation Center for Biosustainability / Technical University of Denmark](http://www.biosustain.dtu.dk/english).

# Contact

[Tilmann Weber](mailto:tiwe@biosustain.dtu.dk)<br>
New Bioactive Compounds Section<br>
The Novo Nordisk Foundation Center for Biosustainability<br>
Technical University of Denmark<br>
Kemitorvet, bygning 220<br>
2800 Kongens Lyngby<br>
Denmark <br>

# License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">The Secondary Metabolite Bioinformatics Portal</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

# If you want to cite the *Secondary Metabolite Bioinformatics Portal*

* [Weber, T. and Kim, H. U., 2016, Synth. Syst. Biotechnol. 1: 69-79](http://www.ncbi.nlm.nih.gov/pubmed/

# Databases focused on biosynthetic gene clusters encoding secondary metabolites

---

[TOC]

---

## antiSMASH database

The antiSMASH database is a comprehensive resource on secondary metabolite biosynthetic gene clusters. It contains gene clusters identified with [antiSMASH](antiSMASH) version 4 on more than 6000 finished bacterial genomes. The antiSMASH database provides a web-interface which allows simple queries as well as a query builder for complex queries. Results can be exported as lists or (if applicable) protein or nucleotide FASTA files. In addition, there are links to the antiSMASH output page for all identified gene clusters

Reference:

* [Blin, K., et al., 2017, Nucleic Acids Res. 45:D555-D559](https://www.ncbi.nlm.nih.gov/pubmed/27924032)
* [Blin, K., et al., 2019, Nucleic Acids Res. 47: D625-D630](http://www.ncbi.nlm.nih.gov/pubmed/30395294)


Link:

* <http://antismash-db.secondarymetabolites.org>

Keywords:
Database, antiSMASH, gene clusters

---

## Bactibase

Bactibase is an integrated open-access database designed for the characterization of bacterial antimicrobial peptides / bacteriocins

References:

* [Hammami, R., et al., 2007, BMC Microbiol. 7:89](http://www.ncbi.nlm.nih.gov/pubmed/17941971)
* [Hammami, R., et al., 2010, BMC Microbiol. 10:22](http://www.ncbi.nlm.nih.gov/pubmed/20105292)

Link:

* <http://bactibase.pfba-lab-tun.org>

---

## ClusterMine360

ClusterMine360 is a database containing more than 250 curated entries of biosynthetic gene clusters including classification of the produced compound(s), taxonomic information of the producers, and links to sequence data and antiSMASH results.

References:

* [Conway, K. R. and Boddy, C. N., 2013, Nucleic Acids Res. 41:D402-7](http://www.ncbi.nlm.nih.gov/pubmed/23104377)
* [Tremblay, N., et al., 2016, Methods Mol. Biol. 1401:233-52](http://www.ncbi.nlm.nih.gov/pubmed/26831712)

Link:

* <http://www.clustermine360.ca/>

Keywords:

database, NRPS, PKS, gene clusters

---

## ClustScan Database / recombinant ClustScan Database (CSDB / r-CSDB)

The ClustScan Database CSDB is a manually curated database containing >160 PKS, NRPS and PKS/NRPS BGCs. The information in the database was partially retrieved from the ClustScan software developed in the same lab.
The recombinant ClustScan Database r-CSDB contains sequences of >20,000 in-silico recombinated gene clusters.

References:

* [Starcevic, A., et al., 2008, Nucleic Acids Res. 36:6882-6892](http://www.ncbi.nlm.nih.gov/pubmed/18978015)
* [Starcevic, A., et al., 2012, J. Ind. Microbiol. Biotechnol. 39:503-11](http://www.ncbi.nlm.nih.gov/pubmed/22042517)
* [Diminic, J., et al., 2013, J. Ind. Microbiol. Biotechnol. 40:653-9](http://www.ncbi.nlm.nih.gov/pubmed/23504028)

Links:

* [ClustScan Database](http://csdb.bioserv.pbf.hr/csdb/ClustScanWeb.html)
* [Recombinant ClustScan Database](http://csdb.bioserv.pbf.hr/csdb/RCSDB.html)

Keywords:
Database, PKS, NRPS, gene clusters

---

## DoBISCUIT

The Database Of BIoSynthesis clusters CUrated and InTegrated contains a literature-based collection of PKS and NRPS biosynthetic gene clusters. The database offers extensive search capabilities on the different features of the clusters. Currently, DoBiSCUIT contains entries for ~100 BGCs.

Reference:

* [Ichikawa, N., et al., 2013, Nucleic Acids Res. 41:D408-14](http://www.ncbi.nlm.nih.gov/pubmed/23185043)

Link:

* <http://www.bio.nite.go.jp/pks/>

Keywords:
database, PKS

---

## IMG-ABC / Integrated Microbial Genomes: Atlas of Biosynthetic Gene Clusters

IMG-ABC is a comprehensive resource of biosynthetic gene clusters identified in microbial genome sequences. The database is fully integrated into JGI's IMG resource and covering more than 23,000 microbial genomes and 2,200 metagenomes. IMG-ABC covers automatically identified gene clusters (using the [ClusterFinder](mining.md#clusterfinder) algorithm), clusters with known biosynthesis products and metabolites.

References:

* [Hadjithomas, M., et al., 2015, MBio 6:e00932](http://www.ncbi.nlm.nih.gov/pubmed/26173699)
* [Hadjithomas, M., et al., 2017, Nucleic Acids Res. 45:D560-D565](http://www.ncbi.nlm.nih.gov/pubmed/27903896)
* [Palaniappan, K., et al., 2020, Nucleic Acids Res. 48: D422-D430](http://www.ncbi.nlm.nih.gov/pubmed/31665416)


Link:

* <https://img.jgi.doe.gov/abc>

Keywords:
Database, genome, metagenome, gene clusters

---

## MIBiG

Please see information on the [MIBiG](mibig) page.

---

## Orphan assembly line polyketide synthases

Catalog of automatically extracted multimodular PKS sequences (including hybrid NRPS/PKS)

Reference:

* [O'Brien, R. V., et al., 2014, J. Antibiot. (Tokyo) 67:89-97](http://www.ncbi.nlm.nih.gov/pubmed/WOS:000330222300013)

Link:

* <http://sequence.stanford.edu/OrphanPKS/>

Keywords: database, PKS
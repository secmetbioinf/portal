# Generic tools

---

[TOC]

---

## autometa

autometa is an automatic binning pipeline to obtain single genomes from metagenomic datasets

References: 

* [Miller, I.J., et al., 2018, bioRxiv, DOI: 10.1101/251462](https://www.biorxiv.org/content/early/2018/01/22/251462)

Links:
* [Source code](https://bitbucket.org/jason_c_kwan/autometa)
* [Docker image](https://hub.docker.com/r/jasonkwan/autometa/)

---

## CRISPy-web

Online tool to design guideRNAs (sgRNAs) for CRISPR applications with microbes. Unlike most other CRISPR design tools, CRISPy-web allows the user to upload custom/non-model organisms genome data to screen for off-target effects. CRISPy-web can directly access [antiSMASH](antismash) results and use the antiSMAS-annotation to aid with the sgRNA design. Version 2 now also suports the CRISPR-BEST base editor technology ([Tong, Y., et al., 2019, Proc. Natl. Acad. Sci. U. S. A. 116: 20366-20375](http://www.ncbi.nlm.nih.gov/pubmed/31548381)
).


References:

* [Ronda, C., et al., 2014, Biotechnol. Bioeng. 111:1604-16](http://www.ncbi.nlm.nih.gov/pubmed/24827782)
* [Blin, K., et al., 2016, Synth. Syst. Biotechnol., 10.1016/j.synbio.2016.01.003d](http://www.sciencedirect.com/science/article/pii/S2405805X15300168)
* [Blin, K., et al., 2020, Synth Syst Biotechnol 5: 99-102](http://www.ncbi.nlm.nih.gov/pubmed/32596519)


Link:

* <https://crispy.secondarymetabolites.org/>

---

## InterProScan

Interpro is a comprehensive resource on protein domains. The web tool InterProScan offers querying the Interpro database and identifying conserved protein domains in query sequences.

References:

* [Mitchell, A., et al., 2015, Nucleic Acids Res. 43:D213-21](http://www.ncbi.nlm.nih.gov/pubmed/25428371)
* [Jones, P., et al., 2014, Bioinformatics 30:1236-40](http://www.ncbi.nlm.nih.gov/pubmed/24451626)
* [Hunter, S., et al., 2009, Nucleic Acids Res. 37:D211-5](http://www.ncbi.nlm.nih.gov/pubmed/18940856)
* [Mulder, N. J. and Apweiler, R., 2008, Curr Protoc Bioinformatics Chapter 2:Unit 2 7](http://www.ncbi.nlm.nih.gov/pubmed/18428686)
* [Mulder, N. and Apweiler, R., 2007, Methods Mol. Biol. 396:59-70](http://www.ncbi.nlm.nih.gov/pubmed/18025686)

Link:

* <http://www.ebi.ac.uk/interpro/>

---

## MultiGeneBlast

The standalone tool MultiGeneBlast is a Blast extension that allows querying sequence databases with complete gene clusters / operons and thus allows the identification of similar pathways in other organisms.

Reference:

* [Medema, M. H., et al., 2013, Mol. Biol. Evol. 30:1218-23](http://www.ncbi.nlm.nih.gov/pubmed/23412913)


Link for download:

* <http://multigeneblast.sourceforge.net/>

---

## PatscanUI

User friendly GUI to the old, but very powerful PATSCAN utility, which is a powerful tool for pattern searching in DNA or amino acid sequences.

References:

* PatScan: [Dsouza, M., et al., 1997, Trends Genet. 13:497-8](http://www.ncbi.nlm.nih.gov/pubmed/9433140)
* GUI: [Blin, K., et al., 2018, Nucleic Acids Res. 46: W205–W208](http://www.ncbi.nlm.nih.gov/pubmed/29722870)

Link:

* GUI: <http://patscan.secondarymetabolites.org>
* GUI source code: <https://github.com/kblin/patscanui>
* PatScan source code and documentation: <http://blog.theseed.org/servers/2010/07/scan-for-matches.html>

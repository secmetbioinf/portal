# antiSMASH

---

[TOC]

---

The **anti**biotics and **S**econdary **M**etabolites **A**nalysis **SH**ell antiSMASH is a fully automated pipeline to mine bacterial and fungal genome date for secondary metabolite biosynthetic gene clusters (BGCs). The small molecules encoded by these BGCs often have various bioactivities including antimicrobial, anti-cancer, anthelminthic and others. Therefore they are lead compounds for many drugs like antibiotics.

antiSMASH was initially developed in a collaborative project between Tübingen University (Tilmann Weber, Kai Blin), Groningen University (Eriko Takano, Rainer Breitling, Marnix Medema) and UCSF (Michael Fischbach).
Currently, antiSMASH development is coordinated at [Wageningen University](http://www.wageningenur.nl/en/Expertise-Services/Chair-groups/Plant-Sciences/Bioinformatics.htm) and the [Novo Nordisk Foundation Center for Biosustainability / Technical University of Denmark](http://www.biosustain.dtu.dk/english)


<img src="../img/antismash.png" style="display: block; margin: 0 auto;">

---

## Link to antiSMASH server	

[http://antismash.secondarymetabolites.org](http://antismash.secondarymetabolites.org "antiSMASH Webserver")

### antiSMASH download

[http://antismash.secondarymetabolites.org/download.html](http://antismash.secondarymetabolites.org/download.html "antiSMASH download page")

### antiSMASH online documentation

[http://docs.antismash.secondarymetabolites.org](http://docs.antismash.secondarymetabolites.org)

### antiSMASH source code repository

*	[antiSMASH source code GIT repository](https://github.com/antismash/antismash "antiSMASH GIT repository")

*	[websmash source code for web component of antiSMASH web server](https://github.com/antismash/websmash)

*	[dispatcher source code for job scheduling component of antiSMASH web server](https://github.com/antismash/dispatcher)

---

## References
* [Blin, K., et al., 2019, Nucleic Acids Res. 47: W81-W87](http://www.ncbi.nlm.nih.gov/pubmed/31032519)

* [Blin, K., et al., 2019, Brief. Bioinform. 20: 1103-1113](http://www.ncbi.nlm.nih.gov/pubmed/29112695)

*	[Blin, K., et al., 2017, Nucleic Acids Res. doi: 10.1093/nar/gkx319](https://www.ncbi.nlm.nih.gov/pubmed/28460038)

*	[Weber, T., et al., 2015, Nucleic Acids Res. 43: W237-W243](http://www.ncbi.nlm.nih.gov/pubmed/25948579)

*	[Blin, K., et al., 2014, PLoS ONE 9: e89420](http://www.ncbi.nlm.nih.gov/pubmed/24586765)

*	[Blin, K., et al., 2013, Nucleic Acids Res. 41: W204-W212](http://www.ncbi.nlm.nih.gov/pubmed/23737449)

*	[Medema, M. H., et al., 2011, Nucleic Acids Res. 39: W339-W346](http://www.ncbi.nlm.nih.gov/pubmed/21672958)

*	[Leclère, V., et al., 2016, Methods Mol Biol. 2016;1401:209-32](https://www.ncbi.nlm.nih.gov/pubmed/26831711)

---

## antiSMASH core developer team (alphabetic)

* Kai Blin
* Hyun Uk Kim
* Marnix Medema
* Simon Shaw
* Tilmann Weber

---

## Contact

* Please use the [Contact Form](http://antismash.secondarymetabolites.org/contact.html) at the antiSMASH homepage.

**Keywords:**

genome mining, ClusterFinder, gene, genome, pathway, gene cluster, secondary metabolite
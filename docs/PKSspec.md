# Tools to analyze PKS specificities

---

[TOC]

---


## Minowa // NRPS/PKS specificity prediction according to Minowa et al.

please see [Minowa et al.](NRPSspec.md#minowa-nrpspks-specificity-prediction-according-to-minowa-et-al) entry in NRPS specificty predictor section.

---

## NRPS/PKS substrate predictor

NRPS/PKS substrate predictor uses a HMM-based approach to predict specificities of NRPS A-domains and PKS AT domains.

Reference:

* [Khayatt, B. I., et al., 2013, PLoS ONE 8:e62136](http://www.ncbi.nlm.nih.gov/pubmed/23637983)

Link:

* <http://www.cmbi.ru.nl/NRPS-PKS-substrate-predictor/>

---

## PKSIIIexplorer

PKSIIIexplorer uses and transductive SVM-approach to identify PKSIII enzymes in datasets
Reference:

* [Vijayan, M., et al., 2011, Bioinformation 6: 125-7](http://www.ncbi.nlm.nih.gov/pubmed/21584189)

Link:

* <http://type3pks.in/tsvm/pks3> (currently service is not available)

---

## Specificity prediction for Type I PKS

Specificity predictions are included in multiple domain identification tools. Please follow the link below.

Link: 

* [Tools to predict PKS and NRPS domain organizations](PKSNRPStools.md)


# MIBiG

---

[TOC]

---

The **M**inimum **I**nformation on **Bi**osynthetic **G**eneclusters standard is a standard approved by the [Genomic Standards Consortium (GSC)](http://gensc.org), defining a minimal set of information to describe BGCs.
While developing the MIBiG standard, a huge number >500 BGCs have ben manually re-annotated by expertes.

## Link to the MIBiG website

* <http://mibig.secondarymetabolites.org/index.html>

## Direct link to MIBiG data

* <http://mibig.secondarymetabolites.org/repository>

## Download of MIBiG data

* <http://mibig.secondarymetabolites.org/download>

---

## Reference

* [Kautsar, S. A., et al., 2020, Nucleic Acids Res. 48: D454-D458](http://www.ncbi.nlm.nih.gov/pubmed/31612915)
* [Medema, M. H., et al., 2015, Nat. Chem. Biol. 11: 625-31](http://www.ncbi.nlm.nih.gov/pubmed/26284661)
* [Epstein, S. C., et al., 2018, Stand Genomic Sci 13: 16](http://www.ncbi.nlm.nih.gov/pubmed/30008988)

---

## Contact

* [Marnix Medema, Wageningen University](mailto:marnix.medema@wur.nl)
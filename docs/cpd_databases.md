# Databases focused on natural products and compounds

---

[TOC]

---

*Please note: In addition to the resources listed here, there exist several commercial databases. If there is no possibility to freely access their data, these databases are not included in this list.*

***

## Antibioticome

Search chemical structure (in SMILE format) against antibioticome to determin potential target of the molecule.

Link:

* <http://magarveylab.ca/antibioticome>

---

## Antimicrobial Peptide Database

The Antimicrobial Peptide Database (APD) contains ~2700 antimicrobial peptides and (small proteins <200 aa) from prokaryotic and eukaryotic organisms. The searchable hand-curated database contains information on the peptide sequences, post-translational modifications, nomenclature, properties, targets, links to peptide sequence records / 3D structures (PDB), when available. In addition to the database, the APD web-page also provides a tools section with links to software and web-pages useful for antimicrobial peptide research.

References:

* [Wang, Z. and Wang, G., 2004, Nucleic Acids Res. 32:D590-2](http://www.ncbi.nlm.nih.gov/pubmed/14681488)
* [Wang, G., et al., 2009, Nucleic Acids Res. 37:D933-7](http://www.ncbi.nlm.nih.gov/pubmed/18957441)
* [Wang, G., et al., 2016, Nucleic Acids Res. 44:D1087-93](http://www.ncbi.nlm.nih.gov/pubmed/26602694)

Link:

* <http://aps.unmc.edu/AP/>

---

## CAMPR3

CAMPR3 also provides information on antimicrobial peptides.

Reference:

* [Waghu, F. H., et al., 2016, Nucleic Acids Res. 44:D1094-7](http://www.ncbi.nlm.nih.gov/pubmed/26467475)

Link:

* <www.camp3.bicnirrh.res.in>

---

## ChEBI

ChEBI (Chemical Entities of Biological Interest) is a general database and ontology of chemical compounds largely focused on small molecules.

References:

* [de Matos, P., et al., 2012, Methods Mol. Biol., 803:273-96](http://www.ncbi.nlm.nih.gov/pubmed/22065232)
* [Hill, D.P., et al., 2013, BMC Genomics 14:513](http://www.ncbi.nlm.nih.gov/pubmed/23895341)
* [Hastings, J., et al., 2013, Nucleic Acids Res. 41(Database issue):D456-63](http://www.ncbi.nlm.nih.gov/pubmed/23180789)
* [Hastings, J., et al., 2016, Nucleic Acids Res. 44:D1214-9](http://www.ncbi.nlm.nih.gov/pubmed/26467479)

Link:

* <https://www.ebi.ac.uk/chebi/>

---

## ChEMBL

ChEMBL is a database providing information on bioactive molecules with drug-like properties.

References:

* [Gaulton, A., et al., 2012, Nucleic Acids Res. 40(Database issue):D1100-7](http://www.ncbi.nlm.nih.gov/pubmed/21948594)
* [McGlinchey, S., et al., 2014, Nucleic Acids Res. 42(Database issue):D1083-90](http://www.ncbi.nlm.nih.gov/pubmed/24214965)
* [Papadatos, G. and Overington, J.P., 2014, Future Med. Chem. 6:361-4](http://www.ncbi.nlm.nih.gov/pubmed/24635517)
* [Davies, M., et al., 2015, Nucleic Acids Res. 43(W1):W612-20](http://www.ncbi.nlm.nih.gov/pubmed/25883136)
* [Gaulton, A., et al., 2017, Nucleic Acids Res. 45:D945-D954](https://www.ncbi.nlm.nih.gov/pubmed/27899562)

Link:

* <https://www.ebi.ac.uk/chembl/>

Keywords:

database, compounds, structures

---

## ChemSpider

ChemSpider is a chemical database providing information on structures and properties of over 35 million structures from hundreds of data sources.

References:

* [Little, J.L., et al., 2012, J. Am. Soc. Mass Spectrom. 23:179-85](http://www.ncbi.nlm.nih.gov/pubmed/22069037)
* ["Editorial: ChemSpider--a tool for Natural Products research", 2015, Nat. Prod. Rep. 32:1163-4](http://www.ncbi.nlm.nih.gov/pubmed/26155872)

Link:

* <http://www.chemspider.com/>

Keywords:

database, compounds, structures

---

## DBAASPv.2

DBAASPv.2 also provides information on antimicrobial peptides.

Reference:

* [Pirtskhalava, M., et al., 2016, Nucleic Acids Res. 44:D1104-12](https://www.ncbi.nlm.nih.gov/pubmed/26578581)

Link:

* <http://dbaasp.org>

---

## KNApSAcK database

The KNApSAcK family of databases contains compound information of more than 50,000 natural products of plants, but also of microorganisms.

References:

* [Afendi, F. M., et al., 2012, Plant Cell Physiol. 53:e1](http://www.ncbi.nlm.nih.gov/pubmed/22123792)
* [Nakamura, Y., et al., 2014, Plant Cell Physiol. 55:e7](http://www.ncbi.nlm.nih.gov/pubmed/24285751)

Links:

* <http://kanaya.aist-nara.ac.jp/KNApSAcK//>
* <http://kanaya.naist.jp/KNApSAcK_Family/>

Keywords:

database, compounds

---

## PubChem

PubChem is a repository of more than 20 million small-molecule compounds, their properties and biological activities. The database contains  synthetic compounds as well as natural products.

References:

* [Bolton, E., et al. (2008). Annual Reports in Computational Chemistry, Volume 4. Washington, DC, American Chemical Society. 4:217-241](ftp://ftp.ncbi.nlm.nih.gov/pubchem/publications/ARCC_PubChem_Integrated_Platform.pdf)
* [Condurso, H. L. and Bruner, S. D., 2012, Nat. Prod. Rep. 29:1099-110](http://www.ncbi.nlm.nih.gov/pubmed/22729219)

Links:

* <http://pubchem.ncbi.nlm.nih.gov>
* <ftp://ftp.ncbi.nlm.nih.gov/pubchem/>

Keywords:

database, compounds

---

## Natural Products Atlas

The Natural Products Atlas is a database aiming to include all published microbial natural products and provides multiple query opportunities. The entries are curated by NP specialists.

Links: 

* <https://www.npatlas.org>

Keywords: 

database, structure, substructure, molecular networks

---

## NPEdia (Natural Products Encyclopedia)

The Natural Products Encyclopedia is a chemical structure-centered database of natural products.

Link:

* <http://www.cbrg.riken.jp/npedia/?LANG=en>

---

## NORINE

NORINE is a curated database on non-ribosomally synthesized peptides, activities and literature references. Currently, NORINE has more than 1,100 entries.

References:

* [Caboche, S., et al., 2008, Nucleic Acids Res. 36:D326-D331](http://www.ncbi.nlm.nih.gov/pubmed/17913739)
* [Caboche, S., et al., 2009, BMC Struct.Biol 9:15](http://www.ncbi.nlm.nih.gov/pubmed/19296847)
* [Caboche, S., et al., 2010, J. Bacteriol. 192:5143-50](http://www.ncbi.nlm.nih.gov/pubmed/20693331)
* [Abdo, A., et al., 2012, J. Comput. Aided Mol. Des. 26:1187-94](http://www.ncbi.nlm.nih.gov/pubmed/23053735)
* [Pupin, M., et al., 2015, Synth. Syst. Biotechnol. 1:89-94](http://www.sciencedirect.com/science/article/pii/S2405805X15300144)
* [Flissi, A., et al., 2016, Nucleic Acids Res. 44:D1113-8](http://www.ncbi.nlm.nih.gov/pubmed/26527733)
* [Leclère, V., et al., 2016, Methods Mol Biol. 1401:209-32](https://www.ncbi.nlm.nih.gov/pubmed/26831711)

Links:

* <http://bioinfo.lifl.fr/norine>

Keywords:

database, compounds, non-ribsomal peptides, activities

---

## Novel Antibiotics Database

The Novel Antibiotics database contains information about compounds that were published in the Journal of Antbiotics until 2008.

Link:

* <http://www.antibiotics.or.jp/journal/database/database-top.htm> Novel Antibiotics Data Base data files download
* <a href="http://www0.nih.go.jp/~jun/NADB/search.html">http://www0.nih.go.jp/~jun/NADB/search.html</a> (online search tool; link currently not working)

Keywords:

database, compounds

---

## NPEdia (Natural Products Encyclopedia)

The RIKEN Chemical Biology Department Natural Products Encyclopedia (RIKEN NPEdia) is a database of natural products, primarily focusing on structural and chemical properties of natural products.

Link:

* <http://www.cbrg.riken.jp/npedia/?LANG=en>

Keywords:

database, compounds, structures, substructure

## StreptomeDB

StreptomeDB is a comprehensive database on metabolites of Streptomycetes. Currently, the database contains entries for more than 4000 natural products, 2500 producers, 900 activities. (For details, see [StreptomeDB statistics page](http://132.230.56.4/streptomedb3/statistics/)

References:

* [Lucas, X., et al., 2013, Nucleic Acids Res. 41:D1130-6](http://www.ncbi.nlm.nih.gov/pubmed/23193280)
* [Klementz, D., et al., 2016, Nucleic Acids Res. 44:D509-14](http://www.ncbi.nlm.nih.gov/pubmed/26615197)

Link:

* <http://www.pharmaceutical-bioinformatics.de/streptomedb2/> (currently offline)
* <http://132.230.56.4/streptomedb3/>

Keywords:

Streptomyces, compounds

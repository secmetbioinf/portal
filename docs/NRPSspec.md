# Tools to analyze NRPS specificities


---

[TOC]

---

## LSI based A-domain function predictor

The LSI based A-domain predictor uses Latent Semantic Indexing to predict Adenylation domain specificities. It accepts single and multi-FASTA sequences as inputs.

Reference:

* [Baranasic, D., et al., 2014, J. Ind. Microbiol. Biotechnol. 41: 461-7](http://www.ncbi.nlm.nih.gov/pubmed/24104398)

Link:

* <http://bioserv7.bioinfo.pbf.hr/LSIpredictor/AdomainPrediction.jsp>

---

## Minowa // NRPS/PKS specificity prediction according to Minowa et al.

The algorithm described in Minowa et al uses a HMM based approach to predict the specificities of NRPS adenylation domains and PKS At domains. In addition, an algorithm is described to predict protein-protein interactions (docking) between PKS/NRPS subunits to extrapolate the products from a whole pathway and not only from PKS/NRPS subunits.

Reference:

* [Minowa, Y., et al., 2007, J. Mol. Biol. 368:1500-1517](http://www.ncbi.nlm.nih.gov/pubmed/17400247)


Link:

* There is no implementation of the original algorithm publicly available; a reimplementation is included in [antiSMASH](antismash.md)

---

## NRPSpredictor/NRPSpredictor2

NRPSpredictor2 uses a Support Vector Machine based approach to classify NRPS adenylation domains according to their substrates and predict the substrate specificity.

References:

* [Rausch, C., et al., 2005, Nucleic Acids Res. 33: 5799-5808](http://www.ncbi.nlm.nih.gov/pubmed/16221976)
* [Röttig, M., et al., 2011, Nucleic Acids Res. 39: W362-367](http://www.ncbi.nlm.nih.gov/pubmed/21558170)

Link:

* <http://nrps.informatik.uni-tuebingen.de>

Link to source code:

* <https://github.com/roettig/NRPSpredictor2>

---

## NRPSsp

NRPSsp used hits against a HMM databases to predict specificities of NRPS adenylation domains.

Reference:

* [Prieto, C., et al., 2012, Bioinformatics 28: 426-7](http://www.ncbi.nlm.nih.gov/pubmed/22130593)
* [Prieto, C., 2016, Methods Mol. Biol. 1401:273-8](http://www.ncbi.nlm.nih.gov/pubmed/26831714)


Link:

* <http://www.nrpssp.com/>

---

## NRPS/PKS substrate predictor

NRPS/PKS substrate predictor uses a HMM-based approach to predict specificities of NRPS A-domains and PKS AT domains.

Reference:

* [Khayatt, B. I., et al., 2013, PLoS ONE 8:e62136](http://www.ncbi.nlm.nih.gov/pubmed/23637983)

Link:

* <http://www.cmbi.ru.nl/NRPS-PKS-substrate-predictor/>

---

## PKS/NRPS Web Server/Predictive Blast Server

The PKS/NRPS Webserver used BLAST to detect catalytic domains in PKS and NRPS. For NRPS, the adenylation domain specificities are predicted by comparing with signatures of Adenylation domains with known substrates (according to [Challis, G. L., et al., 2000, Chem. Biol. 7: 211-224](http://www.ncbi.nlm.nih.gov/pubmed/10712928)).

Reference:

* [Bachmann, B. O. and Ravel, J., 2009, Methods Enzymol. 458: 181-217](http://www.ncbi.nlm.nih.gov/pubmed/19374984)

Link: 

* <http://nrps.igs.umaryland.edu/nrps/>

Link to Source code:

* <http://sourceforge.net/projects/secmetdb/>

---
## SEQL-NRPS

Webserver to predict NRPS A-domain specificities using Sequence Learner (SEQL), a discriminative classification method for sequences.

Reference:

* [Knudsen, M., et al., 2016, Bioinformatics 32: 325-329](http://www.ncbi.nlm.nih.gov/pubmed/26471456)

Link: 

* <http://services.birc.au.dk/seql-nrps/>

---
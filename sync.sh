#!/bin/bash

set -o errexit

git remote update
git rebase origin/master
mkdocs build --clean
rsync -e ssh -avcP --delete site/ root@46.101.106.177:/var/www/html
